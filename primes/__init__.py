from typing import Tuple, Optional, Iterator, Union
from fractions import Fraction

import os
import shelve


class PrimeDBFrame:

    def __init__(self, value: int, terms: Tuple[Tuple[int, int], ...]):
        if not isinstance(value, int):
            raise TypeError(value)
        if not isinstance(terms, tuple):
            raise TypeError(terms)
        if not terms:
            raise ValueError(terms)
        product = 1
        for term in terms:
            if not isinstance(term, tuple):
                raise TypeError(term)
            if len(term) != 2:
                raise ValueError(term)
            factor, power = term
            if not isinstance(factor, int):
                raise TypeError(factor)
            # If the factor is -1, the value has to be negative. If the factor is 0 or 1, the value has to be equal to
            # the factor; we only allow these when they are stand-alone factors. Otherwise, it has to be either 2 or an
            # odd number greater than 2, e.g. something that could potentially be a prime number.
            if (factor < 2 or (factor > 2 and not factor % 2)) and (value >= 0 if factor == -1 else len(terms) != 1):
                raise ValueError(factor)
            if not isinstance(power, int):
                raise TypeError(power)
            if power < 1:
                raise ValueError(power)
            product *= factor ** power
        if product != value:
            raise ValueError(terms)

        self._value = value
        self._terms = terms
        self._next_prime = None

    @property
    def value(self) -> int:
        return self._value

    @property
    def terms(self) -> Tuple[Tuple[int, int], ...]:
        return self._terms

    @property
    def is_prime(self) -> bool:
        return len(self._terms) == 1 and self._value > 1

    @property
    def is_composite(self) -> bool:
        return len(self._terms) > 1 and abs(self._value) > 1

    @property
    def next_prime(self) -> Optional[int]:
        return self._next_prime

    @next_prime.setter
    def next_prime(self, value: int) -> None:
        if not isinstance(value, int):
            raise TypeError(value)
        if value <= self._value or not value % 2:
            raise ValueError(value)
        if self._next_prime is not None:
            raise AttributeError("The next prime for %s has already been set to %s before trying to set it to %s." %
                                 (self._value, self._next_prime, value))
        self._next_prime = value


class PrimeDBConnection:

    def __del__(self) -> None:
        self.close()

    @property
    def is_open(self) -> bool:
        raise NotImplementedError()

    def open(self) -> None:
        raise NotImplementedError()

    def close(self) -> None:
        raise NotImplementedError()

    def put(self, frame: PrimeDBFrame) -> None:
        raise NotImplementedError()

    def get(self, value: int) -> Optional[PrimeDBFrame]:
        raise NotImplementedError()


class ShelvePrimeDBConnection(PrimeDBConnection):

    def __init__(self, path: str, protocol: int = None):
        self._path = os.path.abspath(os.path.expanduser(path))
        self._protocol = protocol
        self._is_open = False
        self._shelf = None  # type: Optional[shelve.Shelf]

    @property
    def path(self) -> str:
        return self._path

    @property
    def protocol(self) -> Optional[int]:
        return self._protocol

    @property
    def is_open(self) -> bool:
        return self._is_open

    def open(self) -> None:
        if not self._is_open:
            if not os.path.isdir(os.path.dirname(self._path)):
                os.makedirs(os.path.dirname(self._path))
            self._shelf = shelve.open(self._path, 'c', self._protocol)
            self._is_open = True

    def close(self) -> None:
        if self._is_open:
            self._shelf.close()
            self._is_open = False

    def put(self, frame: PrimeDBFrame) -> None:
        self.open()
        self._shelf[str(frame.value)] = frame

    def get(self, value: int) -> Optional[PrimeDBFrame]:
        self.open()
        return self._shelf.get(str(value), None)


class PrimeFactorization:

    _connection = ShelvePrimeDBConnection('~/.primes/primes.db')

    @classmethod
    def set_connection(cls, connection: PrimeDBConnection) -> None:
        if connection != cls._connection:
            cls._connection.close()
            cls._connection = connection

    @classmethod
    def close_connection(cls) -> None:
        cls._connection.close()

    @classmethod
    def iter_known_primes(cls) -> Iterator[int]:
        prime = 2
        while prime is not None:
            yield prime
            frame = cls._connection.get(prime)
            prime = None if frame is None else frame.next_prime

    @classmethod
    def iter_primes(cls) -> Iterator[int]:
        prime = 2
        for prime in cls.iter_known_primes():
            yield prime
        last_prime = prime

        last_frame = cls._connection.get(last_prime)  # type: PrimeDBFrame
        if last_frame is None:
            last_frame = PrimeDBFrame(last_prime, ((last_prime, 1),))
            cls._connection.put(last_frame)

        candidate = 3 if last_prime == 2 else last_prime + 2
        while True:
            frame = cls._connection.get(candidate)  # type: PrimeDBFrame
            if frame is None:
                is_prime = True
                for prev_prime in cls.iter_known_primes():
                    if not candidate % prev_prime:
                        is_prime = False
                        break
                    if prev_prime * prev_prime > candidate:
                        break
                if is_prime:
                    frame = PrimeDBFrame(candidate, ((candidate, 1),))
                    cls._connection.put(frame)
                    last_frame.next_prime = candidate
                    cls._connection.put(last_frame)
                    last_frame = frame
                    yield candidate
            elif frame.is_prime:
                last_frame.next_prime = frame.value
                cls._connection.put(last_frame)
                last_frame = frame
                yield frame.value
            candidate += 2

    @classmethod
    def _factor(cls, value: int) -> Tuple[Tuple[int, int], ...]:
        terms = []

        if abs(value) < 2:
            return (value, 1),

        if value < 0:
            terms.append((-1, 1))
            value = -value

        remaining = value
        for prime in cls.iter_primes():
            if prime * prime > remaining:
                break
            count = 0
            while not remaining % prime:
                count += 1
                remaining //= prime
            if count:
                terms.append((prime, count))
        if remaining > 1:
            if cls._connection.get(remaining) is None:
                frame = PrimeDBFrame(remaining, ((remaining, 1),))
                cls._connection.put(frame)
            terms.append((remaining, 1))

        return tuple(terms)

    @classmethod
    def _combine_terms(cls, left: Tuple[Tuple[int, int], ...], right: Tuple[Tuple[int, int], ...], sign: int = 1) \
            -> Tuple[Tuple[int, int], ...]:
        terms_dict = dict(left)
        for factor, power in right:
            terms_dict[factor] = terms_dict.get(factor, 0) + power * sign

        if 0 in terms_dict:
            assert terms_dict[0] >= 0
            if terms_dict[0]:
                return (0, 1),

        if -1 in terms_dict:
            terms_dict[-1] %= 2

        terms = []
        for factor, power in terms_dict.items():
            if factor != 1 and power:
                terms.append((factor, power))
        terms.sort()
        return tuple(terms)

    def __init__(self, value: Union[int, Fraction]):
        self._value = value

        frame = self._connection.get(value.numerator)
        if frame is None:
            frame = PrimeDBFrame(value.numerator, self._factor(value.numerator))
            self._connection.put(frame)
        self._numerator_frame = frame

        frame = self._connection.get(value.denominator)
        if frame is None:
            frame = PrimeDBFrame(value.denominator, self._factor(value.denominator))
            self._connection.put(frame)
        self._denominator_frame = frame

        if self._value.denominator == 1:
            self._terms = self._numerator_frame.terms
        else:
            self._terms = self._combine_terms(self._numerator_frame.terms, self._denominator_frame.terms, -1)

    @property
    def value(self) -> Union[int, Fraction]:
        return self._value

    @property
    def numerator_terms(self) -> Tuple[Tuple[int, int], ...]:
        return self._numerator_frame.terms

    @property
    def denominator_terms(self) -> Tuple[Tuple[int, int], ...]:
        return self._denominator_frame.terms

    @property
    def terms(self) -> Tuple[Tuple[int, int], ...]:
        return self._terms

    def __str__(self) -> str:
        return '*'.join(str(prime) if power == 1 else '(%s**%s)' % (prime, power) for prime, power in self._terms)

    def __repr__(self) -> str:
        return '%s(%r)' % (type(self).__name__, self._value)

    # def __mul__(self, other: Union[int, Fraction, 'PrimeFactorization']) -> 'PrimeFactorization':
    #     if isinstance(other, PrimeFactorization):
    #         other = other.value
    #     elif not isinstance(other, (int, Fraction)):
    #         return NotImplemented
    #     return PrimeFactorization(self.value * other)
    #
    # def __truediv__(self, other: Union[int, Fraction, 'PrimeFactorization']) -> 'PrimeFactorization':
    #     if isinstance(other, PrimeFactorization):
    #         other = other.value
    #     elif not isinstance(other, (int, Fraction)):
    #         return NotImplemented
    #     return PrimeFactorization(self.value / other)


def main():
    PrimeFactorization.set_connection(ShelvePrimeDBConnection('~/.primes/primes.db'))
    while True:
        try:
            value = input('> ').strip()
        except KeyboardInterrupt:
            break
        if not value:
            continue
        for type_ in (int, Fraction, float):
            try:
                value = type_(value)
            except ValueError:
                pass
            else:
                break
        else:
            print("Invalid input: %s" % value)
            continue
        print('%s = %s' % (value, PrimeFactorization(value)))
    return 0
