from setuptools import setup

setup(
    name='primes',
    version='0.0',
    packages=['primes'],
    url='',
    license='MIT',
    author='Aaron Hosford',
    author_email='hosford42@gmail.com',
    description='Primes and prime factorizations database'
)
